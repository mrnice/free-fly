/*
* Free Fly Vario project
*
* Copyright (C) 2017  Bernhard Guillon Bernhard.Guillon@begu.org
*
* This file is part of Free Fly Vario project.
*
* Free Fly Vario is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* Free Fly Vario is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Free Fly Vario. If not, see <http://www.gnu.org/licenses/>.
*
*/

public class SimpleOutput : GLib.Object {

        private NMEADecoder _nmea_decoder;

        void on_simple_output(string msg) {
            print("SIMPLE_OUTPUT:%s", msg);
        }

        public SimpleOutput(NMEADecoder nmea_decoder) {
            _nmea_decoder = nmea_decoder;
            _nmea_decoder.simple_output.connect(on_simple_output);
        }
    }