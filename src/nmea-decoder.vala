/*
* Free Fly Vario project
*
* Copyright (C) 2017  Bernhard Guillon Bernhard.Guillon@begu.org
*
* This file is part of Free Fly Vario project.
*
* Free Fly Vario is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 2 of the License, or
* (at your option) any later version.
*
* Free Fly Vario is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with Free Fly Vario. If not, see <http://www.gnu.org/licenses/>.
*
*/

public class NMEADecoder : GLib.Object {

	private string old_buf="";

	public signal void simple_output(string msg);

	public NMEADecoder() {
        print("NMEADecoder construct\n");
	}

	char get_crc(string message) {
		print("MESSAGE:%s",message);
		char c = 0;
		for (int i = 0; i < message.length; i++)
		{
			c = c ^ message[i];
		}
		return c;
	}

    //FIXME: crc calculation is wrong for now
	bool crc_valid(string nmea_msg) {
		var tokens = nmea_msg.split("\$");
		if(tokens.length < 2)
			return false;
		var nmea_tokens = nmea_msg.split("*");
		print("TOKENS LEN:%i\n", nmea_tokens.length);
		if (nmea_tokens.length != 2)
			return false;
		var crc = (char) int.parse(nmea_tokens[1].replace("\r",""));
		var valid_crc = get_crc(nmea_tokens[0]);
		print("CRC_STR: %s CAL CRC %u, GET CRC %u", nmea_tokens[1].replace("\r",""), crc, valid_crc );
		return (valid_crc == crc);
	}

	public void decode(string token) {
		var strings = token.split("\n");
		print("\nNo of tokens: %i\n",strings.length);
		switch(strings.length) {
			case 0:
				print("FIXME: case 0");
				break;
			case 1:
				print("FIXME: case 1");
				break;
			default:
				print("\nOLD:%s", old_buf);
				print("\nNEW[0]:%s", strings[0]);
				var old_and_new=old_buf+strings[0];
				print("\nOLD+NEW[0]:%s", old_and_new);
				if(crc_valid(old_and_new))
					simple_output(old_buf+strings[0]);
				for (int i=1;i<strings.length-1; i++) {
					print("\nNEW LINE:%s", strings[i]);
					if(crc_valid(strings[i]))
						simple_output(strings[i]);
				}
				old_buf=strings[strings.length-1];
				break;
		}
	}

}
