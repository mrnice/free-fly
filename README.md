## Free Fly Project

This will be a fully flight computer for the Free Fly ESP32 Project [1].

For now this project is just for testing the Free Fly ESP32, which is also compatible with XCsoar [2].
But later it will hopefully grow to a fully flying computer.

[1] https://gitlab.com/mrnice/free-fly-esp32

[2] https://www.xcsoar.org

## Motivation

Because flying makes you feel free this freedom should not stop at flying equipment.

## Installation

    mkdir build
    cd build
    meson ..
    ninja

## License

Free Fly Vario project

Copyright (C) 2017  Bernhard Guillon Bernhard.Guillon@begu.org
 *
This file is part of Free Fly Vario project.
 *
Free Fly Vario is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

Free Fly Vario is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar. If not, see <http://www.gnu.org/licenses/>.
